

@extends('layouts.app')

@section('content')

<body class="bg-gray-100 flex items-center justify-content h-full">
    <div class="container">
        <form
            action="contact"
            method="post"
            class="bg-white p-6 shadow-md"
            style="width: 300px">
            @csrf

            <div class="mb-5">
                <label for="email"
                       class="block text-xs uppercase font-semibold mb-1">
                    Email
                </label>

                <input type="text"
                       id="email"
                       name="email"
                       placeholder="Your email.."
                       class="border px-2 py-1 text-sm w-full">

                @error('email')
                    <div class="text-red-500 text-xs ">
                        {{$message}}
                    </div>
                @enderror
            </div>

            <button class="bg-blue-500 py-2 text-white rounded-full text-sm w-full" type="submit">
                Email me
            </button>

            @if(session('message'))
                <p>
                    {{session('message')}}
                </p>
            @endif

        </form>
    </div>

@endsection
