@component('mail::message')

    Hello {{ $firstname }},
    <p>
        Your signup process has been initiated. Please click the link below to
        complete your registration and start getting paid.
    </p>

    @component('mail::button', ['url' => $url, 'color' => 'blue'])
        Confirm Account Now
    @endcomponent

    Thanks,<br>
    {{ config('app.name') }} Team
@endcomponent
