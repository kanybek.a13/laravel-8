<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ConversationController;
use App\Http\Controllers\ConversationBestReplyController;
use App\Http\Controllers\PagesController;
use App\Http\Controllers\PaymentsController;
use App\Http\Controllers\ContactController;
use App\Http\Controllers\UserNotificationsController;
use App\Http\Controllers\AidarController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// login with id
//auth()->loginUsingId(1);

Auth::routes();

Route::get('/', [PagesController::class, 'index'])->name('index');
Route::get('/home', [PagesController::class, 'home'])->name('home')->middleware('auth');

Route::get('/payments/create', [PaymentsController::class, 'create'])->middleware('auth');
Route::post('/payments', [PaymentsController::class, 'store'])->middleware('auth');

Route::get('/contact', [ContactController::class, 'contact'])->name('contact ');
Route::post('/contact', [ContactController::class, 'store']);

Route::get('/notifications', [UserNotificationsController::class, 'show'])->middleware('auth');

Route::get('/conversations', [ConversationController::class, 'index']);
//Route::get('/conversations/{conversation}', [ConversationController::class, 'show']);
Route::get('conversations/{conversation}', [ConversationController::class, 'show'])->middleware('can:view,conversation');


Route::post('/best-replies/{reply}', [ConversationBestReplyController::class, 'store']);


Route::get('/posts', function (){ return 'This is secret post page';})->middleware('can:view_posts');

Route::get('/aidar/index', [AidarController::class, 'index']);
