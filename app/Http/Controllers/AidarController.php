<?php

namespace App\Http\Controllers;

use App\Models\Ability;
use App\Models\Role;
use App\Models\User;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;

class AidarController extends Controller
{
    public function index(){
        $user = User::find(1);

        $user->assignRole('manager');
        return 1;

//        return $user->abilities();
//
//        $roles = $user->roles();

//        return $roles->get();

//        $abilitiesOfRole = $roles->abilities();
//        return true;

        $role = Role::firstOrCreate([
            'name'=>'manager'
        ]);

        $ability = Ability::firstOrCreate([
            'name'=>'view_posts'
        ]);

        $role->allowTo($ability);
//        $user->assignRole($role);

//        $roles = $user->roles()->get();
//        return  $roles;

        return true;
//        ddd($roles);
    }

//    public function home()
//    {
//        Cache::remember('foo', 60,fn() => 'qwerty');
//
//        return Cache::get('foo');
//    }

    public function home(Filesystem $file){
        return View::make('home');

//        return $file->get(public_path('robots.txt'));
//        return File::get(public_path('robots.txt'));
  }

}
