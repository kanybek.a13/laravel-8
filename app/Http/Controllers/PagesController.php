<?php

namespace App\Http\Controllers;

use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Request;

class PagesController extends Controller
{
    public function index(){
//        return view('welcome');

        return View::make('welcome');
    }

//    public function home()
//    {
//        Cache::remember('foo', 60,fn() => 'qwerty');
//
//        return Cache::get('foo');
//    }

    public function home(Filesystem $file){
        return View::make('home');

//        return $file->get(public_path('robots.txt'));
//        return File::get(public_path('robots.txt'));
  }

}
