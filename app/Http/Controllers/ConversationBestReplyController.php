<?php

namespace App\Http\Controllers;

use App\Models\Reply;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

class ConversationBestReplyController extends Controller
{
    public function store(Reply $reply)
    {
//        $this->authorize('update', $reply->conversation);
//
//        $reply->conversation->best_reply_id = $reply->id;
//        $reply->conversation->save;
//
//        return back();
//
        $this->authorize('update', $reply->conversation);
//        $this->authorize($reply->conversation);

        $reply->conversation->setBestReply($reply);

        return back();
    }
}
