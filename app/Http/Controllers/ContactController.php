<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use App\Mail\ContactMe;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\View;

class ContactController extends Controller
{
    //
    public function contact()
    {
        return View::make('/contact');
    }

    public function store()
    {
        request()->validate(['email' => 'required|email']);

        Mail::to(\request('email'))
            ->send(new Contact());

//        Mail::raw('it works', function ($message){
//            $message->to(\request('email'))->
//                subject('Hello');
//        });

        return redirect('/contact')
            ->with('message', "Email sent!");

        return View::make('/contact');
    }
}
