<?php

namespace App\Http\Controllers;

use App\Events\ProductPurchased;
use App\Notifications\PaymentReceived;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\View;

class PaymentsController extends Controller
{
    //
    public function create()
    {
//        return view('payments.create');
        return View::make('/payments.create');
    }

    public function store()
    {
        ProductPurchased::dispatch('toy');
//        event(new ProductPurchased('toy'));
    }

/*
    public function store()
    {
//        If notifying a collection of user
//        Notification::send(request()->user(), new PaymentReceived());

//      If notifying a one user
//        $user->notify(new PaymentReceived(100));
        request()->user()->notify(new PaymentReceived(100));

        return redirect('/payments/create');
    }
*/
}
